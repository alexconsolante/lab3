//Alexander Consolante
//ID: 1931671

package linearalgebra;

public class Vector3d {

    private double x;
    private double y;
    private double z;

    public Vector3d(double var_x,double var_y,double var_z) {

        this.x = var_x;
        this.y = var_y;
        this.z= var_z;

    }

    public double getX() {

        return this.x;
    }

    public double getY() {

        return this.y;
    }

    public double getZ() {

        return this.z;
    }

    public double magnitude(){

        return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)+Math.pow(this.z,2));

    }

    public double dotProduct(Vector3d vector2) {

        double result;
        result = ((this.x*vector2.x)+(this.y*vector2.y)+(this.z*vector2.z));
        return result;

    }

    public Vector3d add(Vector3d var_vector) {

        Vector3d newVector = new Vector3d(this.x+var_vector.x,this.y+var_vector.y,this.z+var_vector.z);
        return newVector;
    }

    public String toString() {

        return "X: "+this.x+" Y: "+this.y+" Z: "+this.z;
    }

}