// Alexander Consolante
// ID:1931671
package linearalgebra;
import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {

    @Test

    public void testGetters() {

        Vector3d vector1 = new Vector3d(2,4,4);
        assertEquals(2,vector1.getX(),0.01);
        assertEquals(4,vector1.getY(),0.01);
        assertEquals(4,vector1.getZ(),0.01);
    }

    @Test

    public void testMagnitude() {

        Vector3d vector1 = new Vector3d(2,4,4);
        //assertEquals(8.0,vector1.magnitude(),0.01);
        assertEquals(6.0,vector1.magnitude(),0.01);

    }

    @Test

    public void testDotProduct() {

        Vector3d vector1 = new Vector3d(2,4,6);
        Vector3d vector2 = new Vector3d(3,6,8);
        //assertEquals(35.0,vector1.dotProduct(vector2),0.01);
        assertEquals(78.0,vector1.dotProduct(vector2),0.01);


    }

    @Test

    public void testAdd() {

        Vector3d correctVector = new Vector3d(5,10,14);
        //Vector3d incorrectVector = new Vector3d(10,15,12);
        Vector3d vector1 = new Vector3d(2,4,6);
        Vector3d vector2 = new Vector3d(3,6,8);
        //assertEquals(incorrectVector.toString(),vector1.add(vector2).toString());
        assertEquals(correctVector.toString(),vector1.add(vector2).toString());
    }

}
